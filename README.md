# General Information

This repository contains the data and the accompanying data analysis for the paper:

Brocher et al, *Using task effort and pupil size to track covert shifts of visual attention independently of a pupillary light reflex*, submitted to: Behavioral Research Methods

# Pupil size data

## Recording

* Recorded with SR Research Eyelink 1000 Tower Mounted Eyetracker.  
* Sampling rate of 250 Hz.   
* All recordings were collected from the right eye 
* The experiments were presented using ExperimentBuilder.
* Raw data is safed in binary edf file

## Preprocessing

* Data from all participants is exported into a text-format sample report using DataViewer
* These sample reports and all further steps will be provided in this repository
* All preprocessing relies on the functions contained in **functions.R**  

# Experiments

## Experiment 1

### Description

This experiment included the the factor *angle* as an experimental predictor.
The factor has 5 levels representing different angles in which the target pictures were presented.

### Codebooks for variables contained in the data files

**RECORDING_SESSION_LABEL**: Unique participant ID

**TRIAL_INDEX**: Index of the current trial


**SAMPLE_INDEX**: Index of the current sample 

**item**: Unique item ID  

**winkel**: Experimental Condition indicating the angle, in which the target pictures were presented (Values: 12.5°, 20.0°, 27.5°, 35°, 42.5°) 

**RIGHT_PUPIL_SIZE**: Recorded pupil size at the current sample (arbitrary unit) 

**Exp_VARIABLEAntwort1**: Response given by participant at the first prompt (Range 1-4)

**pfeil**: Indicator, whether the first response prompt refers to the target picture on the left or right (*links.png* indictates left, *rechts.png* indicates right) 

**Exp_VARIABLEAntwort2**: Response given by participant at the first prompt (Range 1-4) 

**koord_links**: Screen coordinates of the left target picture

**koord_rechts**: Screen coordinates of the left target picture

**helledreieckelinks**: Number of white triangles on the left target picture (Correct response for left side)

**helledreieckerechts**: Number of white triangles on the left target picture (Correct response for right side)

**links**: Filename of the left target picture. Also encodes number of white triangles

**rechts**: Filename of the right target picture. Also encodes number of white triangles

### Files

* **raw_data_exp1.csv**: File contains the raw data
* **tc_plot_data_exp1.csv**: File contains the data after artifact correction used for the timecourse plot. File is created during analysis.  
* **behav_data_exp1.csv**: File contains the data used for the analysis of the response. File is created during analysis.
* **exp1_analysis.r**: Main analysis file, which contains the preprocessing of the data and the statistical analysis for the pupil size data. 
* **exp1_timecourse_plot.R**: File contains the code for the timecourse plot.
* **exp1_behavioral_analysis.R**: File contains the code for the analysis of the responses. 

## Experiment 2

### Description

As Experiment 1, this experiment included the the factor *angle* as an experimental predictor.
The factor now has only 3 levels representing different angles in which the target pictures were presented.
In addition it included the factor *task*, which either required the participant to count the number of white triangles or to determine, whether there were white triangles at all

### Codebooks for variables contained in the data files

**RECORDING_SESSION_LABEL**: Unique participant ID

**TRIAL_INDEX**: Index of the current trial
  
**SAMPLE_INDEX**: Index of the current sample 

**item**: Unique item ID  

**winkel**: Experimental Condition indicating the angle, in which the target pictures were presented (Values: 12.5°, 27.5°, 42.5°) 

**prompt**: Experimental Condition indicating whether white triangles had to be only detected (Value: Aufmerksamkeit) or counted (Value: Wahrnehmung)

**RIGHT_PUPIL_SIZE**: Recorded pupil size at the current sample (arbitrary unit) 

**Exp_VARIABLEAntwort1**: Response given by participant at the first prompt (Range 1-4)

**pfeil**: Indicator, whether the first response prompt refers to the target picture on the left or right (*links.png* indictates left, *rechts.png* indicates right) 

**Exp_VARIABLEAntwort2**: Response given by participant at the first prompt (Range 1-4) 

**koord_links**: Screen coordinates of the left target picture

**koord_rechts**: Screen coordinates of the left target picture

**helledreieckelinks**: Number of white triangles on the left target picture (Correct response for left side)

**helledreieckerechts**: Number of white triangles on the left target picture (Correct response for right side)

**links**: Filename of the left target picture. Also encodes number of white triangles

**rechts**: Filename of the right target picture. Also encodes number of white triangles



### Files

* **raw_data_exp2.csv**: File contains the raw data
* **tc_plot_data_exp2.csv**: File contains the data after artifact correction used for the timecourse plot. File is created during analysis.  
* **behav_data_exp2.csv**: File contains the data used for the analysis of the response. File is created during analysis.
* **exp2_analysis.r**: Main analysis file, which contains the preprocessing of the data and the statistical analysis for the pupil size data. 
* **exp2_timecourse_plot.R**: File contains the code for the timecourse plot.
* **exp2_behavioral_analysis.R**: File contains the code for the analysis of the responses. 


## Experiment 3

### Description

As Experiment 1, this experiment included the the factor *angle* as an experimental predictor.
The factor now has only 3 levels representing different angles in which the target pictures were presented.
In addition it included the factor *contrast*, which either indicated whether the target picture was in high or low contrast

### Codebooks for variables contained in the data files

**RECORDING_SESSION_LABEL**: Unique participant ID

**TRIAL_INDEX**: Index of the current trial
  
**SAMPLE_INDEX**: Index of the current sample 

**item**: Unique item ID  

**winkel**: Experimental Condition indicating the angle, in which the target pictures were presented (Values: 12.5°, 27.5°, 42.5°) 

**prompt**: Experimental Condition indicating whether the target pictures were in high or low contrast (Values: high, low)

**RIGHT_PUPIL_SIZE**: Recorded pupil size at the current sample (arbitrary unit) 

**Exp_VARIABLEAntwort1**: Response given by participant at the first prompt (Range 1-4)

**pfeil**: Indicator, whether the first response prompt refers to the target picture on the left or right (*links.png* indictates left, *rechts.png* indicates right) 

**Exp_VARIABLEAntwort2**: Response given by participant at the first prompt (Range 1-4) 

**koord_links**: Screen coordinates of the left target picture

**koord_rechts**: Screen coordinates of the left target picture

**helledreieckelinks**: Number of white triangles on the left target picture (Correct response for left side)

**helledreieckerechts**: Number of white triangles on the left target picture (Correct response for right side)

**links**: Filename of the left target picture. Also encodes number of white triangles

**rechts**: Filename of the right target picture. Also encodes number of white triangles



### Files

* **raw_data_exp3.csv**: File contains the raw data
* **tc_plot_data_exp3.csv**: File contains the data after artifact correction used for the timecourse plot. File is created during analysis.  
* **behav_data_exp3.csv**: File contains the data used for the analysis of the response. File is created during analysis.
* **exp3_analysis.r**: Main analysis file, which contains the preprocessing of the data and the statistical analysis for the pupil size data. 
* **exp3_timecourse_plot.R**: File contains the code for the timecourse plot.
* **exp3_behavioral_analysis.R**: File contains the code for the analysis of the responses. 

