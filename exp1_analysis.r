# Analysis Experiment 1

# Preprocessing

# Loading packages and pupil functions Run for package installation:
# install.packages(c("data.table", "ggplot2", "lme4", "scales", "stringr",
# "effects"))

library(data.table)
library(ggplot2)
library(lme4)
library(scales)
library(foreign)
library(stringr)
library(effects)

# The functions for preprocessing and retrieving the pupil measure are contained
# in the functions.R file
source('functions.R', echo = FALSE)

# The functions for preprocessing and retrieving the pupil measure are contained
# in the functions.R file
hilflos_raw <- fread("raw_data_exp1.csv", na.strings = ".")

# change decimal delimiter from , to .
hilflos_raw[, RIGHT_PUPIL_SIZE := as.numeric(sub(",", ".", RIGHT_PUPIL_SIZE, fixed = TRUE))]

# The raw data file is really large, so only the relevant columns are kept and described in the codebook

columns <- c("RECORDING_SESSION_LABEL", "TRIAL_INDEX", "SAMPLE_INDEX", "item",  "winkel", "RIGHT_PUPIL_SIZE", "Exp_VARIABLEAntwort1" , "pfeil", "Exp_VARIABLEAntwort2", "koord_links", "koord_rechts", "helledreieckelinks", "helledreieckerechts", "links", "rechts" )  

hilflos_raw <- hilflos_raw[, columns, with = FALSE]

#-------------------------------------------------------------------------------

# PUPIL DATA ARTIFACT DETECTION AND TRIAL REJECTION
# Detect Artifacts for every trial of every participant
hilflos_raw[, is_artifact := find_artifacts(RIGHT_PUPIL_SIZE), .(RECORDING_SESSION_LABEL, TRIAL_INDEX) ]

# Evaluate how many data points are artifacts per trial
hilflos_raw[, exclude_trial:= evaluate_data_loss(is_artifact), .(RECORDING_SESSION_LABEL, TRIAL_INDEX) ]

# Evalute subject performance
hilflos_raw[, exclude_subject := evaluate_participant_performance(exclude_trial), .(RECORDING_SESSION_LABEL)]

#-----------------------------------------------------------------------------------

# ARTIFACT AND REJECTION SUMMARY
# total number of sample containing artifacts or data loss
hilflos_raw[,.N, .(is_artifact) ]

# number of rejected trials
hilflos_raw[SAMPLE_INDEX==1,.N, .(exclude_trial) ]

# number of rejected participants
hilflos_raw[SAMPLE_INDEX==1 & TRIAL_INDEX ==1,.N, .(exclude_subject) ]

#-------------------------------------------------------------------------------

# DATA CLEANUP
# Exclude subjects

hilflos_clean <- hilflos_raw[exclude_subject == FALSE]

# Exclude trials
hilflos_clean <- hilflos_clean[exclude_trial == FALSE]

# Recode remaining artifacts as NA
hilflos_clean <- hilflos_clean[is_artifact==TRUE, RIGHT_PUPIL_SIZE:=NA]

#-------------------------------------------------------------------------------

# Behavioral data
# evaluate whether the responses for the first and second prompt were correct 
hilflos_clean[, correct_response_a := FALSE][, correct_response_b := FALSE]
hilflos_clean[pfeil == "links.png", `:=`(correct_response_a = (helledreieckelinks == Exp_VARIABLEAntwort1),
  correct_response_b = (helledreieckerechts == Exp_VARIABLEAntwort2))]
hilflos_clean[pfeil == "rechts.png", `:=`(correct_response_a = (helledreieckerechts == Exp_VARIABLEAntwort1),
  correct_response_b = (helledreieckelinks == Exp_VARIABLEAntwort2))]

# write this as a file to use later in the script for the behavioral analysis
# only include one sample per participant and trial to avoid redundancy
fwrite(x = hilflos_clean[SAMPLE_INDEX == 10], file = "behav_data_exp1.csv")

#-------------------------------------------------------------------------------
# CALCULATION OF MEASUREMENTS I: BASELINE
# Calculate baseline peak pupil size (pps)
# For 250 Hz Sampling Rate, the first 63 samples contain the baseline

# How long (time/samples) does each interval last?
# 250ms Baseline 1:63 
# 250ms PreCues 64:126
# 200ms blank   127:177
# 300ms Cues    178:253 
# 1750ms Messung 254:691
# 200 ms Pfeil     692:742

# Presentation of Precues is the baseline

hilflos_clean[,baseline_pps_precues := get_pupil_avg(RIGHT_PUPIL_SIZE, region = c(64,126), avg_window = c(-12,12)), .(RECORDING_SESSION_LABEL, TRIAL_INDEX)]

# substract baseline pps from all samples
# From here on, all samples are deviation from baseline
hilflos_clean[,pupil_size_blc_precues := RIGHT_PUPIL_SIZE-baseline_pps_precues]

# adjust time axis, so that 0 is the onset of the precues
hilflos_clean[, time := SAMPLE_INDEX - 63]

# Convert samples to ms
hilflos_clean[, shifted_time := (SAMPLE_INDEX - 63) * 4]

# write this state to another file to use for timecourse plotting

fwrite(hilflos_clean, "tc_plot_data_exp1.csv")

#-------------------------------------------------------------------------------
# Statistical inference

# Calculating the dependent variable for inferential statistics
hilflos_stats <- hilflos_clean[, .(dev_bl_psize = get_pupil_avg(pupil_size_blc_precues, region = c(254,691), avg_window = c(-12,12))), .(RECORDING_SESSION_LABEL, TRIAL_INDEX, winkel, item, correct_response_a, correct_response_b, rechts, links)]

# Display the descriptives for the conditions 
hilflos_stats[, .(cond_mean = mean(dev_bl_psize, na.rm = T), cond_sd = sd(dev_bl_psize,  na.rm = T)), .(winkel) ]

# Statistical inference using Mulitilevel linear regression
# Looking at the predictor angle
hilflos_stats[, winkel := as.factor(winkel)]

# Null model includes fixed predictors and the appropriate multilevel structure
hl_model_null <- lmer(dev_bl_psize ~ 1 + 
                          (1|RECORDING_SESSION_LABEL)+(1|item) , data = hilflos_stats, REML = FALSE)

# Angle mode includes angle a fixed predictor
hl_model_angle <- lmer(dev_bl_psize ~ winkel + 
                        (1|RECORDING_SESSION_LABEL)+(1|item) , data = hilflos_stats, REML = FALSE)

# Compare the models
anova(hl_model_null, hl_model_angle)

#-------------------------------------------------------------------------------
# Analysing eccentricity

# Counting how many white symbols are on each side
# This is coded within the filenames contained in the variables links and rechts
hilflos_stats[, `:=`(white_left = str_count(links, "w"), white_right = str_count(rechts, "w"))]

# Calculate the complete count of white symbols
hilflos_stats[, white := white_left + white_right]



# We treat the variable white a numerical variable, which is centered at the overall mean of white. Thereby regression coefficients have to be interpreted as deviations from this mean value 
hilflos_stats[, brightness := scale(white, scale=FALSE)]
hilflos_stats[, angle := as.factor(winkel)]

# fitting the models

hl_model_white <- lmer(dev_bl_psize ~ brightness + 
        (1|RECORDING_SESSION_LABEL)+(1|item) , data = hilflos_stats, REML = FALSE)

hl_model_no_int <- lmer(dev_bl_psize ~ angle + brightness + 
    (1|RECORDING_SESSION_LABEL)+(1|item) , data = hilflos_stats, REML = FALSE)

hl_model <- lmer(dev_bl_psize ~  angle * brightness + 
        (1|RECORDING_SESSION_LABEL)+(1|item) , data = hilflos_stats, REML = FALSE)

# Comparing the models in a bottom up fashion

anova(hl_model_angle, hl_model_no_int)
anova(hl_model_angle, hl_model)

# effects plot

plot(allEffects(hl_model), xlab="Brightness (centered at mean number of white objects)"
     , ylab = "Pupil size: Deviation from baseline",
     x.var="brightness", 
     lattice=list(layout=c(5,1)), main="",
     axes=list(x=list(rotate=45)))

